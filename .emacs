;; Paul McQuade .emacs File 
;;
; start package.el with emacs
(require 'package)
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(custom-set-variables
 ;;
 ;; Step 1 : Set theme to light-blue
 ;;
 ;;
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (light-blue)))
 ;;
 ;; Step 2 : install packages/Extensions
 ;;
 ;; 
'(package-archives
   (quote
    (("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa-stable" . "https://stable.melpa.org/packages/"))))
;;
;; auto-complete
;;
'(package-selected-packages (quote (company rtags ggtags flycheck iedit emmet-mode auto-complete))))
(custom-set-faces
 )
;;
;;An auto-complete source for C/C++ header files
;;
(require 'auto-complete-config)
(ac-config-default)
;;
;; yasnippet configuration
;;
(require 'yasnippet)
(yas-global-mode 1)
;;
;; emmet configuration
;;
(require 'emmet-mode)
(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(add-hook 'html-mode-hook 'emmet-mode)
(add-hook 'css-mode-hook  'emmet-mode)

;; Fix iedit bug in Linux
(define-key global-map (kbd "C-c ;") 'iedit-mode)
;;
;; Semantic Mode
(semantic-mode 1)
;;
;; flycheck (Syntax Checking)
;;
(global-flycheck-mode)
;;
;; Emacs Powerline 
;;
;; URL: https://github.com/jonathanchu/emacs-powerline
;;
(add-to-list 'load-path "~/.emacs.d/vendor/emacs-powerline")
(require 'powerline)

(set-face-attribute 'mode-line nil
                    :foreground "Black"
                    :background "DarkOrange"
                    :box nil)
;;
;; GNU Global source code tagging system
;;
(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))

;;
;; Set company-mode Hook
;;
(add-hook 'after-init-hook 'global-company-mode)
