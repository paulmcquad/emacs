;;; -*- no-byte-compile: t -*-
(define-package "rtags" "2.21" "A front-end for rtags" 'nil :commit "1249950963e494fbd66a4138cef639ffe6e05cd2" :authors '(("Jan Erik Hanssen" . "jhanssen@gmail.com") ("Anders Bakken" . "agbakken@gmail.com")) :maintainer '("Jan Erik Hanssen" . "jhanssen@gmail.com") :url "http://rtags.net")
